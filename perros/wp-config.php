<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'dogs');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h,`9=q1__db.oivWUf_n|7f|~+XSL%>zlS/I!&/*VK wvmf4(4u?L~B``5,?*|Y2');
define('SECURE_AUTH_KEY',  'W;|+uVY ]pSRee?Rqs#$hRnl3.!c&-rR|#O?;n @d<$(CjAo7{=-@rX3,$#,~|,+');
define('LOGGED_IN_KEY',    '((e:Vx[hB,>/(:cvL<A}cAX2S=@5#L267;S]+V6FsDXM!@w<vXd%dJ>}k,r<l#dY');
define('NONCE_KEY',        'baDc)ynbKQ:}|@?D0Gk0POPN7Gd m(lwe^{.|SV|$9+Xmp*d-l1e*IX++2]I{npk');
define('AUTH_SALT',        'v;Xs.w,S#yz@foCZ=HaG-rA][_|~OEN2;n,XW{P1oEZ(;G,R/$=g#r-6q~ge_:?#');
define('SECURE_AUTH_SALT', 'CI1(-cBJ&n,]6[D/<C|85&x3]r<7%},0&rX&hqUq`FL]oFPH11$47etJmt};ZE|:');
define('LOGGED_IN_SALT',   'q}b.=R[TO:W-T}3uh|q|r#_<-Blw- bHy;z=](?|qU*xHN/eiV&oG-6+|O Lf-7N');
define('NONCE_SALT',       '+PI+=J}lW8u1pkv$wR{MIwFp|sL@~6t^dh<|:wFJ8;UILKcd3Kt]<[Gbc#|Yjzd_');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'pctv_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

